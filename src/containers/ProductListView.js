import React,{Component} from 'react';
import Product from '../components/Product';
import axios from 'axios';

const listData = [];
for (let i = 0; i < 23; i++) {
    listData.push({
        href: 'http://ant.design',
        title: `ant design part ${i}`,
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        content: 'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
    });
}

export default class ProductList extends Component {
    
    state = {
        products: [],
        
    }

    obtener3 = (producto) => {
            //console.log(producto);
            this.props.obtener(producto);
      };

    componentDidMount() {
        axios.get('http://127.0.0.1:8000/Api/ListaProductos/')
        .then((result) => {
            this.setState({
                products: result.data
            })
            console.log(result.data);
        });
        
    }
    
   

    render() {
        return (
            <Product data={this.state.products} obtener3={this.obtener3}>
                
            </Product>
            );
        }
    }