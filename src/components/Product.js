import React,{Component} from "react";
import { List, Typography , Card  } from 'antd';
import AgregarVenta from './AgregarVenta';


const { Title } = Typography;
const { Text } = Typography;
const { Paragraph } = Typography;
const { Meta } = Card;

const listData = [];
for (let i = 0; i < 23; i++) {
	listData.push({
		href: 'http://ant.design',
		title: `ant design part ${i}`,
		avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
		description: 'Ant Design, a design language for background applications, is refined by Ant UED Team.',
		content: 'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
	});
}

const disabled = true;



class Product extends Component {
	
	obtener2 = (producto) => {
		//console.log("Soy producto y vendiste esto:"+ producto);
		this.props.obtener3(producto);
	};
	
	
	
	
	render() {
		return(
			<List
			grid={{ gutter: 16, column: 3 }}
			itemLayout="vertical"
			size="large"
			pagination={{
				onChange: (page) => {
					console.log(page);
				},
				pageSize: 3,
			}}
			
			dataSource={this.props.data}
			
			footer={<div><b> Ferreteria Ipiales</b>|Productos disponibles para venta</div>}
			renderItem={item => (
				
				
				
				<List.Item>
				<div >
				<Card
				style={{ width: 300 }}
				cover={<img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}>
				
				<Meta
				title={<Title level={3}>{item.descripcion} </Title>}
				/>
				<div>
				
				<Text strong>Cantidad Maxima que se puede vender: <b> {item.stock_minimo}</b></Text> <br />
				<Paragraph ><b>Ubicacion:</b> Estante -> {item.estante}</Paragraph>
				</div>
				<br />
				<div>
				
				<AgregarVenta descripcion = {item.descripcion} max={item.stock_minimo} precio={item.precio_unit} referencia={item.referencia} obtener2={this.obtener2}
				
				/>
				
				</div>
				</Card>
				</div>
				</List.Item>
				
				)}
				/>
				
				)
			}
		}
		export default Product;
		
		
		
		
		
		