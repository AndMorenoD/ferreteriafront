import React,{Component} from 'react';
import { Form,  Button, InputNumber , Typography  } from 'antd';
import uuid from 'uuid';
const { Text } = Typography;

let cantidadForm = 1;
function onChangeCantidad(value) {
  return cantidadForm=value;
}


const listaProductos = [];

export default class AgregarVenta extends Component {
  
  AgregarProductoVenta = e =>{
    e.preventDefault();
    
    const producto = {
      id: uuid(),
      cantidad : cantidadForm,
      precio : this.props.precio,
      referencia : this.props.referencia,
      descripcion : this.props.descripcion,
      
      
    }
    
    listaProductos.push(producto);
    this.props.obtener2(producto);
    
  }
  
  render() {
    return (
      
      <Form onSubmit={this.AgregarProductoVenta} >

      <Text strong>Cant. Producto a vender</Text> <br />
      <InputNumber   min={1} max={this.props.max} defaultValue={1} onChange={onChangeCantidad} /><br />
      <div>
      <br />
      <Button type="primary" htmlType="submit" block>Vender</Button>
      </div>
      </Form>
      );
    }
  }