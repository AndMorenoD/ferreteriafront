import React,{Component} from 'react';
import {
  List,PageHeader, Tag, Tabs, Button, Statistic, Row, Col
} from 'antd';

import ProductoVendido from './ProductoVendido';


const Description = ({ term, children, span = 12 }) => (
  <Col span={span}>
  <div className="description">
  <div className="term">{term}</div>
  <div className="detail">{children}</div>
  </div>
  </Col>
  );
  
  const content = (
    <Row>
    <Description term="Vendedor"><a>Andres Moreno</a></Description>
    <Description term="ID de vendedor">
    <a>421421</a>
    </Description>
    <Description term="Creation Time">2017-01-10</Description>
    <Description term="Effective Time">2017-10-10</Description>
  
    </Row>
    );
    
    const extraContent = (
      <Row>
      <Col span={12}>
      <Statistic title="Estado de venta" value="Pendiente" />
      </Col>
      <Col span={8}>
      <br />
      <br />
   
      <Statistic title="Total a Vender" prefix="$" value={0} />
      </Col>
      </Row>
      );
      
      export default class VentaTotal extends Component {
        
        render() {
          
          
          const productosVendidos = this.props.productosVendidos;
          const mensaje = Object.keys(productosVendidos).length === 0 ? 'No hay productos en la lista de venta' : 'Factura de productos que se venderán.';
          const mensajeTag = Object.keys(productosVendidos).length === 0 ? 'Sin productos' : 'Productos en proceso de venta';
          let color = Object.keys(productosVendidos).length === 0 ? 'red' : 'green';
          return(
        
            <List
            header={<div>
              <PageHeader
              onBack={() => window.history.back()}
              title={mensaje}
              tags={<Tag color={color}>{mensajeTag}</Tag>}
              extra={[
                <Button key="1">Administrar ventas</Button>,
              ]}
              
              >
              <div className="wrap">
              <div className="content padding">{content}</div>
              <div className="extraContent">{extraContent}</div>
              </div>
              </PageHeader>,
              </div>}
              pagination={{
                onChange: (page) => {
                  console.log(page);
                },
                pageSize: 5,
              }}
              bordered
              dataSource={productosVendidos}
              renderItem={item => (
                <List.Item>
                
                <ProductoVendido
                cantidad = {item.cantidad} referencia={item.referencia} precio={item.precio} descripcion = {item.descripcion}
                
                />
                
                </List.Item>)}
                
                />
                
                )
              }
            }
            
            
            