import React,{Component} from 'react';
import {
  Card,Icon ,Typography 
} from 'antd';

const { Text } = Typography;

const gridStyle = {
  width: '25%',
  textAlign: 'center',
};

let valorUnitarioTotal = 0;

export default class ProductoVendido extends Component {

  render() {
    valorUnitarioTotal = parseInt(this.props.precio) * parseInt(this.props.cantidad);
    return (
      <Card
      size="small"
      title={this.props.descripcion}
      extra={<Icon type="close" />}
      style={{ width: 1300 }}>
      <Card.Grid style={gridStyle}><Text type="success">Referencia: {this.props.referencia}</Text></Card.Grid>
      <Card.Grid style={gridStyle}>Cantidad Vendida: <Text type="danger">{this.props.cantidad}</Text></Card.Grid>
      <Card.Grid style={gridStyle}>Precio Unitario: <Text type="danger">{this.props.precio}</Text></Card.Grid>
      <Card.Grid style={gridStyle}>Valor total del producto: <Text type="danger">{valorUnitarioTotal}</Text></Card.Grid>
      
      
      </Card>
      );
    }
  }