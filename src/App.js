import React, { Component } from 'react';
import 'antd/dist/antd.css';
import CustomLayout from './containers/Layout';
import ProductList from './containers/ProductListView';
import VentaTotal from './components/VentaTotal';
class App extends Component {

  state = {
    productosVendidos: []
  }

  obtener = (producto) => {
    //console.log("TE ESCUHO TE SIENTO Y TE OIGO LIST");
    const productosVendidos = [producto, ...this.state.productosVendidos ];
    console.log(productosVendidos)

    this.setState({
      productosVendidos
    });
  }

  render() {
    return (
      <div className="App">
        
        <CustomLayout >
          <VentaTotal productosVendidos={this.state.productosVendidos}> </VentaTotal> <br />
          <ProductList obtener={this.obtener}></ProductList>
        </CustomLayout>
      </div>
          
    );
  }
}

export default App;
